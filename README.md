# awesome-misc

<img src="https://awesome.re/badge-flat2.svg" alt="Awesome">


> A collection of miscellaneous useful links I've found relating to my work, hobbies and interests.

## Contents


- [Cycling](#cycling)
- [Music](#music)
  - [Recording](#recording)
  - [Ableton Live](#ableton-live)
- [Software](#software)
  - [Go](#go)
  - [Protocol Buffers](#protocol-buffers)
  - [Postgres](#postgres)
  - [Rust](#rust)
- [Design](#design)
  - [Iconography](#iconography)
  - [Design Systems](#design-systems)

## Resources

### Cycling
* [GCN: The 5 minute bike wash](https://www.youtube.com/watch?v=QvzVRxlIUL0) - The magic WD-40 advice.
* [GCN: How To Index Your Gears - Adjusting Your Rear Derailleur](https://www.youtube.com/watch?v=Bbk5RcH0bbQ) - The comprehensive guide to rear indexing.

### Music

#### Recording
* [Andrew Graves Tracking a song](https://www.youtube.com/watch?v=aLIqvUF4YNw) - Long live Arcane Roots.
* [How To Record A Song From Scratch - Vocals](https://www.youtube.com/watch?v=CqNwY4vSg_g) - Good and insightful advice on simple vocal recording.

#### Ableton Live
* [YouTube: Reid Stefan](https://www.youtube.com/channel/UC4K6tc2C0hauYw5SoXNtkbA) - The best puppet in the business when it comes to super useful Ableton tips.

### Software

#### Go

* [HTTP Server example from Go Playground](https://github.com/golang/playground/blob/master/server.go) - A nice example of the Server struct approach.
* [Dave Cheney - Practical Go](https://dave.cheney.net/practical-go) - Great tips from a Golang guru.
* [How I Write HTTP Web Services after Eight Years](https://www.youtube.com/watch?v=rWBSMsLG8po) - Some great advice on HTTP server structure from another Go guru.
* [go-structure-examples](https://github.com/katzien/go-structure-examples) - Some more interesting ideas on structure from Kat Zien.
* [golang-standards/project-layout](golang-standards/project-layout) - A collation of the emerging and recommended standards in Go projects.
* [Go Proverbs](https://www.youtube.com/watch?v=PAAkCSZUG1c) - Rob Pike's classic talk on the Go ethos.
* [Go at SoundCloud](http://peter.bourgon.org/go-best-practices-2016/) - A nice Go talk from engineering lessons learned at Soundcloud. 

#### Protocol Buffers

* [buf](https://buf.build/docs/introduction) - A great library for linting and working with Protocol Buffers efficiently.

#### Postgres

* [Go/Postgresql Timestamps](http://lpar.ath0.com/2017/11/go-postgresql-time-zones/) - A great article on the mysteries of UTC timestamps in Postgres when using Go.

#### Rust

* [Half-hour to learn Rust](https://fasterthanli.me/blog/2020/a-half-hour-to-learn-rust/) - A nice no-nonsense primer on Rust syntax.

### Design

#### Iconography

* [7 Principals of icon design](https://uxdesign.cc/7-principles-of-icon-design-e7187539e4a2) - A nice little article on design icon.

####  Design Systems
* [GitHub: Primer](https://primer.style/) - GitHub's CSS design system.
*  [GOV.UK Design System](https://design-system.service.gov.uk/) - GOV.UK Styles and Components.
*  [Bulb: Solar](https://design.bulb.co.uk/styles/colours) - Bulb's Solar design system.
